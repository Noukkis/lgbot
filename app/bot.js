const	Discord = require('discord.js');

class Bot {
	
	constructor(config) {
		this.config = config;
		this.client = new Discord.Client();
		
	}
	
	start() {
		this.client.login(this.config.token);
		this.client.on('message', (msg) => msg.reply(msg.content));
	}
}

module.exports = Bot; 
